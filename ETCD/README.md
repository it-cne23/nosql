# NoSQL
https://gitlab.com/ch-tbz-wb/Stud/NoSQL/-/blob/main/2_Unterrichtsressourcen/etcd/Assignments.md

# Cloud Init
Das Cloud Init wird zum aufsetzen der Virtuellen Maschine verwendet. Dieses Cloud Init installiert alle nötigen resourcen und die dazugehörigen komponenten.

# Python Script
Das Python script kann durch optionale argumente keys auslesen und schreiben.

# Einstellungen
Wichtig ist, dass auf der Virtuellen Maschine die Ports nach aussen offen sind. 
Der folgende Command updated the System Firewall. Azure/AWS müsste zusätzlich im Portal angepasst werden.
```
sudo iptables -A INPUT -p tcp --dport 2379 -j ACCEPT
```

Zudem müsste in der etcd.conf folgende Zeile angepasst werden.
```
advertise-client-urls: http://0.0.0.0:2379
listen-client-urls: http://0.0.0.0:2379
```

