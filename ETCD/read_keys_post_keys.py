# ------------------------------------------------------------------------
# SAMPLE EXECUTION
# python read_keys_post_keys.py get --etcd-host localhost --etcd-port 2379
# python read_keys_post_keys.py put --etcd-host localhost --etcd-port 2379
# ------------------------------------------------------------------------

import etcd3
import click

@click.group()
def cli():
    pass

@cli.command()
@click.option('--etcd-host', default='localhost', help='Hostname or IP address of the etcd server.')
@click.option('--etcd-port', default=2379, help='Port of the etcd server.')
@click.option('--key', prompt='Enter the key to put into etcd', help='Key to put data into etcd.')
@click.option('--value', prompt='Enter the value for the key', help='Value to store in etcd for the given key.')
def put(etcd_host, etcd_port, key, value):
    client = etcd3.client(host=etcd_host, port=etcd_port)
    client.put(key, value)
    print(f'Successfully put data into etcd: Key={key}, Value={value}')

@cli.command()
@click.option('--etcd-host', default='localhost', help='Hostname or IP address of the etcd server.')
@click.option('--etcd-port', default=2379, help='Port of the etcd server.')
@click.option('--key', prompt='Enter the key to read from etcd', help='Key to read data from etcd.')
def get(etcd_host, etcd_port, key):
    client = etcd3.client(host=etcd_host, port=etcd_port)
    value, _ = client.get(key)
    if value:
        print(f'Value found in etcd for key={key}: {value.decode("utf-8")}')
    else:
        print(f'No value found in etcd for key={key}')

if __name__ == '__main__':
    cli()

