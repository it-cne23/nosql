# MONGODB

Das aufsetzen der Datenbank geschah über AWS EC2 mit dem unten stehenden cloud init.
Wichtig ist, dass das OS Ubuntu ausgewählt wird.

```
#cloud-config
users:
  - name: ubuntu
    sudo: ALL=(ALL) NOPASSWD:ALL
    groups: users, admin
    home: /home/ubuntu
    shell: /bin/bash
    ssh_authorized_keys:
      - ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDRI4IqTl6RNGbOJlpVLT6qSuRa34FqHtdOQaq1vxgTPPDesHMGrTIM2jaS2S6HgKMgW3C8COFXR23SQAcyF2C9G+H4MzTge/dmpFDJh3ram2N7HC/N1tB76W0nA05aQWvDHAHSqqB20pAstxvtq8DteGERCNj6sl9GRKDxwXdAthuRXwq+dM05cq6T7vDki/yMmJUYRmyGBEzKfci6XYBMQ079I/4x33NGGqsK23wFb3khqziCUTRtCwJe7frX9Z1ak2JG9RMdEf4l/5kGjOez0VptMcpdZM4C7uyklFtGIb9OjAPLHowgq0cCB9bL7h86U5FkdejLkC87Uzhm/Fo7 teacher-key
ssh_pwauth: false
disable_root: false    
package_update: true 
packages:
  - unzip
  - gnupg
  - curl
write_files:
  - path: /home/ubuntu/mongodconfupdate.sh
    content: |
      sudo sed -i 's/#security:/security:\n  authorization: enabled/g' /etc/mongod.conf
  - path: /home/ubuntu/mongodbuser.txt
    content: |
      use admin;
      db.createUser(
        {
          user: "admin",
          pwd: "MyPassword.45",
          roles: [
            { role: "userAdminAnyDatabase", db: "admin" },
            { role: "readWriteAnyDatabase", db: "admin" }
          ]
        }
      );

runcmd:
  - curl -fsSL https://pgp.mongodb.com/server-6.0.asc | sudo gpg -o /usr/share/keyrings/mongodb-server-6.0.gpg --dearmor
  - echo "deb [ arch=amd64,arm64 signed-by=/usr/share/keyrings/mongodb-server-6.0.gpg ] https://repo.mongodb.org/apt/ubuntu jammy/mongodb-org/6.0 multiverse" | sudo tee /etc/apt/sources.list.d/mongodb-org-6.0.list
  - sudo apt-get update -y
  - sudo apt-get install -y mongodb-org
  - sudo sed -i 's/127.0.0.1/0.0.0.0/g' /etc/mongod.conf
  - sudo chmod +x /home/ubuntu/mongodconfupdate.sh
  - sudo /home/ubuntu/mongodconfupdate.sh
  - sudo systemctl restart mongod
  - sudo mongosh < /home/ubuntu/mongodbuser.txt

```

# Erstellung der Datenbank und Collections

ich habe mich für eine Datenbank entschieden, welche eine Serie beeinhaltet. Die verschachtelung gibt an, welche Episoden es gibt. 
Zudem wird eine zweite Collection erstellt, welche dann die Schauspieler beeinhaltet. Zu beachten ist, dass nach der erstellung der Collections noch die ObjectID der Serie in der Charakter Collection eingefühgt werden muss um eine Referenz zu erhalten.

Shows:
```json
{
  "name": "Dark",
  "genre": "Science Fiction, Drama, Mystery",
  "release_year": 2017,
  "country": "Germany",
  "episodes": [
    {
      "episode_number": 1,
      "title": "Geheimnisse",
      "air_date": "2017-12-01",
      "duration": "60 minutes"
    },
    {
      "episode_number": 2,
      "title": "Lügen",
      "air_date": "2017-12-01",
      "duration": "60 minutes"
    },
    {
      "episode_number": 3,
      "title": "Vergangenheit",
      "air_date": "2017-12-01",
      "duration": "60 minutes"
    },
    {
      "episode_number": 4,
      "title": "Doppelleben",
      "air_date": "2017-12-01",
      "duration": "60 minutes"
    },
    {
      "episode_number": 5,
      "title": "Der Mord",
      "air_date": "2017-12-01",
      "duration": "60 minutes"
    },
    {
      "episode_number": 6,
      "title": "Sic Mundus Creatus Est",
      "air_date": "2017-12-01",
      "duration": "60 minutes"
    },
    {
      "episode_number": 7,
      "title": "Kreuzwege",
      "air_date": "2017-12-01",
      "duration": "60 minutes"
    },
    {
      "episode_number": 8,
      "title": "As You Sow, so You Shall Reap",
      "air_date": "2017-12-01",
      "duration": "60 minutes"
    },
    {
      "episode_number": 9,
      "title": "Everything is Now",
      "air_date": "2017-12-01",
      "duration": "60 minutes"
    },
    {
      "episode_number": 10,
      "title": "Alpha und Omega",
      "air_date": "2017-12-01",
      "duration": "60 minutes"
    }
  ]
}
```

Charakter:
```json
[
  {
    "name": "Jonas Kahnwald",
    "actor": "Louis Hofmann",
    "role": "Protagonist",
    "series": "Dark",
    "episodes_appeared": [
      {
        "episode_number": 1,
        "episode_title": "Geheimnisse"
      },
      {
        "episode_number": 2,
        "episode_title": "Lügen"
      },
      {
        "episode_number": 3,
        "episode_title": "Vergangenheit"
      },
      {
        "episode_number": 4,
        "episode_title": "Doppelleben"
      },
      {
        "episode_number": 5,
        "episode_title": "Der Mord"
      },
      {
        "episode_number": 6,
        "episode_title": "Sic Mundus Creatus Est"
      },
      {
        "episode_number": 7,
        "episode_title": "Kreuzwege"
      },
      {
        "episode_number": 8,
        "episode_title": "As You Sow, so You Shall Reap"
      },
      {
        "episode_number": 9,
        "episode_title": "Everything is Now"
      },
      {
        "episode_number": 10,
        "episode_title": "Alpha und Omega"
      }
    ]
  },
  {
    "name": "Martha Nielsen",
    "actor": "Lisa Vicari",
    "role": "Protagonist",
    "series": "Dark",
    "episodes_appeared": [
      {
        "episode_number": 1,
        "episode_title": "Geheimnisse"
      },
      {
        "episode_number": 2,
        "episode_title": "Lügen"
      },
      {
        "episode_number": 3,
        "episode_title": "Vergangenheit"
      },
      {
        "episode_number": 4,
        "episode_title": "Doppelleben"
      },
      {
        "episode_number": 5,
        "episode_title": "Der Mord"
      },
      {
        "episode_number": 6,
        "episode_title": "Sic Mundus Creatus Est"
      },
      {
        "episode_number": 7,
        "episode_title": "Kreuzwege"
      },
      {
        "episode_number": 8,
        "episode_title": "As You Sow, so You Shall Reap"
      },
      {
        "episode_number": 9,
        "episode_title": "Everything is Now"
      },
      {
        "episode_number": 10,
        "episode_title": "Alpha und Omega"
      }
    ]
  },
  {
    "name": "Ulrich Nielsen",
    "actor": "Oliver Masucci",
    "role": "Protagonist",
    "series": "Dark",
    "episodes_appeared": [
      {
        "episode_number": 1,
        "episode_title": "Geheimnisse"
      },
      {
        "episode_number": 2,
        "episode_title": "Lügen"
      },
      {
        "episode_number": 3,
        "episode_title": "Vergangenheit"
      },
      {
        "episode_number": 4,
        "episode_title": "Doppelleben"
      },
      {
        "episode_number": 5,
        "episode_title": "Der Mord"
      },
      {
        "episode_number": 6,
        "episode_title": "Sic Mundus Creatus Est"
      },
      {
        "episode_number": 7,
        "episode_title": "Kreuzwege"
      },
      {
        "episode_number": 8,
        "episode_title": "As You Sow, so You Shall Reap"
      },
      {
        "episode_number": 9,
        "episode_title": "Everything is Now"
      },
      {
        "episode_number": 10,
        "episode_title": "Alpha und Omega"
      }
    ]
  }
]
```

# Aggregationsabfragen für MongoDB

1. **$match**: Finde alle Charaktere, die in der Serie "Dark" erscheinen.
   
   ```json
   db.characters.aggregate([
     { $match: { series: ObjectId("6642538e03a7d11f9a3ed4df") } }
   ])
   ```

2. **$group**: Gruppiere die Charaktere nach ihrem Land und zähle, wie viele Charaktere aus jedem Land stammen.
   
   ```json
    db.characters.aggregate([
    { $group: { _id: "$country", count: { $sum: 1 } } }
    ])
   ```

3. **$match**: Finde alle Charaktere, die in der Serie "Dark" erscheinen und aus Deutschland kommen.

   ```json
    db.characters.aggregate([
    { $match: { series: ObjectId("6642538e03a7d11f9a3ed4df"), country: "Germany" } }
    ])
   ```

4. **$project**: Projiziere nur den Namen und die Rolle der Charaktere.

   ```json
    db.characters.aggregate([
    { $project: { _id: 0, name: 1, role: 1 } }
    ])
   ```

5. **$sort**: Sortiere die Charaktere nach ihrem Geburtsdatum absteigend.

   ```json
    db.characters.aggregate([
    { $sort: { birth_date: -1 } }
    ])
   ```

6. **$limit**: Zeige nur die ersten 5 Charaktere an.

   ```json
    db.characters.aggregate([
    { $limit: 5 }
    ])
   ```

7. **$sum**: Berechne die Anzahl der Episoden, in denen jeder Charakter erscheint.

   ```json
    db.characters.aggregate([
    { $unwind: "$episodes_appeared" },
    { $group: { _id: "$name", total_episodes: { $sum: 1 } } }
    ])
   ```

8. **$lookup**: Verknüpfe die Charaktere mit den Serien, in denen sie erscheinen.

   ```json
    db.characters.aggregate([
    { $lookup: { from: "series", localField: "series", foreignField: "_id", as: "series_info" } }
    ])
   ```

9. **$unwind**: Entpacke die Liste der Episoden, damit sie einzeln betrachtet werden können.

   ```json
    db.characters.aggregate([
    { $unwind: "$episodes_appeared" }
    ])
   ```

